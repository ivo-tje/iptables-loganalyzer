# iptables loganalyzer
Place some logs in the given directory (/tmp/iptables by default) and run the script.
This will count the lines and sort them on frequency.

The script is compatible with both iptables and ip6tables.
Easy for analyzing newly implemented iptables for missing rules ;)
