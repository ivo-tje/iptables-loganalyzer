#!/usr/bin/perl
#
# V0.1 - Ivo Schooneman -  Initial script - 20121112
#
#

use strict;         # We love strict coding!
use autodie;        # Always welcome when i forget a 'or die $!';

my $log_dir = qw(/tmp/iptables);    # The directory where the logfiles are...
my @data;                               # The array where the data end.
my %totals;                             # The hash were the totals end.

opendir (DIR, $log_dir) or die $!;      # Open the log_dir and loop trough the files
while (my $file = readdir(DIR)) {
    next unless ($file !~ m/^\./);              # Skip files starting with a dot (also . and .. dir)
    open FILE, $log_dir."/".$file or die $!;    # Open the file and loop trough it line by line
    while (my $line = <FILE>) {
        my %vals;
        my @tmp = split(' ', $line);            # Split the line by space in the array tmp
        foreach my $t (@tmp) {                  # Start filling the hash based on the splitted lines using regexp
            if($t =~ m/^IN/) { my @x = split('=', $t); $vals{'IN'} = $x[1]; };      # Using array x as a temp to split on the = and only get the second part, the real data
            if($t =~ m/^OUT/) { my @x = split('=', $t); $vals{'OUT'} = $x[1]; };
            if($t =~ m/^SRC/) { my @x = split('=', $t); $vals{'SRC'} = $x[1]; };
            if($t =~ m/^DST/) { my @x = split('=', $t); $vals{'DST'} = $x[1]; };
            if($t =~ m/^TYPE/) { my @x = split('=', $t); $vals{'TYPE'} = $x[1]; };
            if($t =~ m/^PROTO/) { my @x = split('=', $t); $vals{'PROTO'} = $x[1]; };
            if($t =~ m/^DPT/) {
                my @x = split('=', $t);
                if($x[1] =~ m/(137|138|139)/) { # These ports are windows crap, filtering...
                   for (keys %vals) {           # Throw away the whole line if it matches the windows crap filter.
                       delete $vals{$_};
                   };
                   next;
                };
                $vals{'DPT'} = $x[1];
            };
        };
        if(keys %vals) {    # Only add hash to array if it's not empty (filtered items are empty ;)
            push @data,{%vals};
        };
    };
};
closedir(DIR);

for my $i ( 0 .. $#data ) {     # Count the totals for each reccord! Generate a $countline as key, the counter will be the value.
    my $countline = $data[$i]{IN}.",".$data[$i]{OUT}.",".$data[$i]{SRC}.",".$data[$i]{DST}.",".$data[$i]{DPT}.$data[$i]{TYPE}.",".$data[$i]{PROTO};
    if ($totals{$countline}) {
        $totals{$countline}++;
    }else{
        $totals{$countline}=1;
    };
};

print "COUNT,INTERFACE-IN,INTERFACE-OUT,SOURCE,DESTINATION,PORT,PROTOCOL\n"; # Print a comma seperated list of the results
foreach my $key (sort { $totals{$b} <=> $totals{$a} } keys %totals) {
   printf $totals{$key}.",".$key."\n";
};
